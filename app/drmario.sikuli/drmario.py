# start the emulator
# pause it
# wait for "Ready!", then unpause

import time

bugs = {
	'b': ["b1.png", "b2.png"],
	'y': ["y1.png", "y2.png"],
	'r': ["r1.png", "r2.png"],
}

pills = {
	'b': [Pattern("left-b.png").exact(), Pattern("right-b.png").exact()],
	'r': [Pattern("left-r.png").exact(), Pattern("right-r.png").exact()],
	'y': [Pattern("left-y.png").exact(), Pattern("right-y.png").exact()],
}

pillDrop = Region(find('pill-insert-area.png'))


# Slowed down typing so repeated keys aren't missed
def hit(key, times):
	for i in range(times):
		keyDown(key)
		time.sleep(0.02)
		keyUp(key)
		time.sleep(.24)

def printBottle():
	global bottle
	for y in range(16):
		for x in range(8):
			print bottle[x][y],
		print ''

def getThisPill():
	pill = ['_', '_']
	for i in range(2):
		for name, images in pills.items():
			if pillDrop.exists(images[i], 0):
				pill[i] = name
				break
	return pill

def getTops():
	global bottle
	tops = ['_']*8
	for x in range(8):
		for y in range(16):
			if bottle[x][y] != '_':
				tops[x] = bottle[x][y]
				break

	
	return tops

def getOptimal(pill, tops):
	bestScore = -1
	bestPosition = -1
	for x in range(7):
		tmpScore = 0
		for i in range(2):
			if pill[i] == tops[x+i]:
				tmpScore = tmpScore + 1
			elif tops[x+i] != '_':
				tmpScore = tmpScore - .5
				
		if tmpScore > bestScore:
			bestScore = tmpScore
			bestPosition = x

	return bestPosition


bottle = []
def findViruses():
	global bottle

	bottle = [x[:] for x in [['_']*16]*8]

	bottleTop = find("bottle-top.png")
	bottleArea = bottleTop.below(256)
	for name, images in bugs.items():
		for bug in images:
			try:
				found = bottleArea.findAll(bug)
				while found.hasNext():
					match = found.next()
					x = (match.x-bottleArea.x) / 16
					y = (match.y - bottleArea.y) / 16
					bottle[x][y] = name
			except:
				pass


flag = False
def processPill(event):
	global flag
	flag = not flag
	if flag: return

	keyUp(Key.DOWN)
	print 'I saw a change!'
	
	time.sleep(0.25)
	hit('p', 1)
	print "Detecting current pill..."
	pill = getThisPill()
	if pill[0] == '_' or pill[1] == '_':
		print "False positive :("
		return

	print "Identifying virii..."
	findViruses()

	print "Calculating move..."
	tops = getTops()
	moves = getOptimal(pill, tops) - 3

	print "** Bottle:"
	printBottle()
	print "** Tops:"
	print ' '.join(tops)
	print "** Moves (%s):" % (''.join(pill))
	print moves
	print ""

	if moves < 0:
		key = Key.LEFT
		moves = moves * -1
	else:
		key = Key.RIGHT

	hit('p', 1)
	time.sleep(0.25)
	hit(key, moves)
	keyDown(Key.DOWN)


pillDrop.onChange(1, processPill)
Settings.ObserveScanRate = 10
print("Ready!")
pillDrop.observe(FOREVER)


